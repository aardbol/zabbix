#!/usr/bin/python
"""
Author: Leonardo Malik
Year: 2020
Version: 1.3
License: LGPL 2.1

Functions based off of code of https://github.com/lukecyca/pyzabbix (LGPL 2.1)
"""

import socket
import struct
import subprocess
import sys

# Proxy or server directly
SERVER_HOST = ""
PORT = 10051
DEBUG = False
HOST = socket.gethostname()

if len(sys.argv) > 1:
    if sys.argv[1] == "-d":
        DEBUG = True
    if len(sys.argv) == 3:
        HOST = sys.argv[2]

if DEBUG:
    print "Server/Proxy: " + SERVER_HOST + ":" + str(PORT)
    print "Hostname: " + HOST


def create_packet(request):
    data_len = struct.pack('<Q', len(request))
    packet = 'ZBXD\x01' + data_len + request

    def ord23(x):
        if not isinstance(x, int):
            return ord(x)
        else:
            return x

    return packet


def create_request(messages):
    msg = []

    for m in messages:
        msg.append('{"host":"' + m[0] + '","key":"' + m[1] + '","value":"' + str(m[2]) + '"}')

    msgstr = ",".join(msg)

    request = '{"request":"sender data","data":[' + msgstr + ']}'
    request = request.encode("utf-8")

    return request


def chunk_send(irequest):
    packet = create_packet(irequest)
    host_addr = (SERVER_HOST, PORT)

    try:
        # IPv4
        connection_ = socket.socket(socket.AF_INET)
    except socket.error:
        # IPv6
        try:
            connection_ = socket.socket(socket.AF_INET6)
        except socket.error:
            raise Exception("Error creating socket for " + host_addr[0] + ":" + str(host_addr[1]))

    connection = connection_
    connection.settimeout(10)

    try:
        # server and port must be tuple
        connection.connect(host_addr)
        connection.sendall(packet)
    except socket.timeout:
        connection.close()
        raise socket.timeout
    except socket.error, err:
        # In case of error we should close connection, otherwise
        # we will close it after data will be received.
        connection.close()
        raise err


##
# Get memory stats and convert to bytes
##

proc = subprocess.Popen(['cat', '/proc/meminfo'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', 'MemTotal'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $2}'], stdin=proc.stdout, stdout=subprocess.PIPE)
total_ram = int(proc.stdout.read()) * 1024
proc = subprocess.Popen(['cat', '/proc/meminfo'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', 'MemFree'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $2}'], stdin=proc.stdout, stdout=subprocess.PIPE)
avail_ram = int(proc.stdout.read()) * 1024
pavail_ram = round(float(avail_ram) / float(total_ram) * 100, 4)

if DEBUG:
    print "total_ram: " + str(total_ram)
    print "avail_ram: " + str(avail_ram)
    print "pavail_ram: " + str(pavail_ram)

proc = subprocess.Popen(['cat', '/proc/meminfo'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', 'SwapTotal'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $2}'], stdin=proc.stdout, stdout=subprocess.PIPE)
total_swap = int(proc.stdout.read().decode()) * 1024
proc = subprocess.Popen(['cat', '/proc/meminfo'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', 'SwapFree'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $2}'], stdin=proc.stdout, stdout=subprocess.PIPE)
free_swap = int(proc.stdout.read()) * 1024
pfree_swap = round(float(free_swap) / float(total_swap) * 100, 4)

if DEBUG:
    print "total_swap: " + str(total_swap)
    print "free_swap: " + str(free_swap)
    print "pfree_swap: " + str(pfree_swap)

##
# Get CPU stats
##

proc = subprocess.Popen(['cat', '/proc/cpuinfo'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', '-m', '1', 'cpu cores'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $NF}'], stdin=proc.stdout, stdout=subprocess.PIPE)
total_cpus = proc.stdout.read().strip()
proc = subprocess.Popen(['uptime'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $(NF-2)}'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['sed', '-e', 's/,//'], stdin=proc.stdout, stdout=subprocess.PIPE)
loadavg_1m = proc.stdout.read().strip()
proc = subprocess.Popen(['uptime'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $(NF-1)}'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['sed', '-e', 's/,//'], stdin=proc.stdout, stdout=subprocess.PIPE)
loadavg_5m = proc.stdout.read().strip()
proc = subprocess.Popen(['uptime'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{print $NF}'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['sed', '-e', 's/,//'], stdin=proc.stdout, stdout=subprocess.PIPE)
loadavg_15m = proc.stdout.read().strip()

if DEBUG:
    print "total_cpus: " + total_cpus
    print "loadavg_1m: " + loadavg_1m
    print "loadavg_5m: " + loadavg_5m
    print "loadavg_15m: " + loadavg_15m

##
# root filesystem stats
##

proc = subprocess.Popen(['df', '-i'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', '-E', '%[ ]+/$'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{if (NF == 5) print $4; else print $5}'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['sed', '-e', 's/%//'], stdin=proc.stdout, stdout=subprocess.PIPE)
free_inodes = round(100 - float(proc.stdout.read()), 4)
proc = subprocess.Popen(['df'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', '-E', '%[ ]+/$'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{if (NF == 5) print $4; else print $5}'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['sed', '-e', 's/%//'], stdin=proc.stdout, stdout=subprocess.PIPE)
space_util = round(float(proc.stdout.read()), 4)
proc = subprocess.Popen(['df'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', '-E', '%[ ]+/$'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{if (NF == 5) print $1; else print $2}'], stdin=proc.stdout, stdout=subprocess.PIPE)
total_space = int(proc.stdout.read()) * 1024
proc = subprocess.Popen(['df'], stdout=subprocess.PIPE)
proc = subprocess.Popen(['grep', '-E', '%[ ]+/$'], stdin=proc.stdout, stdout=subprocess.PIPE)
proc = subprocess.Popen(['awk', '{if (NF == 5) print $2; else print $3}'], stdin=proc.stdout, stdout=subprocess.PIPE)
used_space = int(proc.stdout.read()) * 1024

if DEBUG:
    print "free_inodes on /: " + str(free_inodes)
    print "space_util on /: " + str(space_util)
    print "total_space on /: " + str(total_space)
    print "used_space on /: " + str(used_space)

###

data = [
    [HOST, "vm.memory.size[total]", total_ram],
    [HOST, "vm.memory.size[available]", avail_ram],
    [HOST, "vm.memory.size[pavailable]", pavail_ram],
    [HOST, "system.swap.size[,free]", free_swap],
    [HOST, "system.swap.size[,pfree]", pfree_swap],
    [HOST, "system.swap.size[,total]", total_swap],
    [HOST, "system.cpu.num", total_cpus],
    [HOST, "system.cpu.load[all,avg1]", loadavg_1m],
    [HOST, "system.cpu.load[all,avg5]", loadavg_5m],
    [HOST, "system.cpu.load[all,avg15]", loadavg_15m],
    [HOST, "vfs.fs.inode[/,pfree]", free_inodes],
    [HOST, "vfs.fs.size[/,pused]", space_util],
    [HOST, "vfs.fs.size[/,total]", total_space],
    [HOST, "vfs.fs.size[/,used]", used_space],
]

chunk_send(create_request(data))
